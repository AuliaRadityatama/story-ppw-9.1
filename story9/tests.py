from django.test import TestCase, Client
from django.urls import resolve
from .views import signup
from django.contrib.auth.models import User
# Create your tests here.

home_template = 'home.html'
signup_template = 'signup.html'

class TestStory9(TestCase):
    def test_landing_page_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)
    
    def test_using_home_html(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, home_template)

    def test_using_signup_html(self):
        response = Client().get('/signup/signup/')
        self.assertTemplateUsed(response, signup_template)

    def test_using_func(self):
        found = resolve('/signup/signup/')
        self.assertEqual(found.func, signup)

    def test_header_ada(self):
        response = Client().get('/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Selamat datang di website Story 9!", html_kembalian)
        self.assertTemplateUsed(response, home_template)

    def test_title_ada(self):
        response = Client().get('/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Story 9", html_kembalian)
        self.assertTemplateUsed(response, home_template)

    def test_register(self):
        Client().post('/signup/signup/', {'username':'pewepewe', 'first_name': 'pewe', 'last_name': 'ajah', 'email': 'pewe777@gmail.com', 'password1':'4kunp3w3', 'password2':'4kunp3w3'}, format='text/html')
        jumlah = User.objects.count()
        self.assertEqual(jumlah, 1)
