from django.urls import path
from django.conf.urls import url
from .views import signup

app_name = 'story9'

urlpatterns = [
    path('signup/',signup, name='signup'),
]